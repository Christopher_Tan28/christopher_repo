//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
     //empty output, fill this so that it can print onto the page.
    
    //Question 1 here 
    
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    let output = [54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19,
-51, -17, -25];
    let posOdd = [];
    let negEven = [];
    for(let i=0; i < output.length; i++){
        if(output[i] % 2!==0 && output[i]>=0)
        {
           posOdd.push(output[i])
        }
        else if (output[i] % 2===0 && output[i]<0)
        {
            negEven.push(output[i]); 
        }
        else
            {
                
            }
        outPutArea.innerHTML = "Positive Odd: " + posOdd + "\n"+ "Negative Even: " + negEven
    }
}

function question2(){
    let freq1 = 0;
    let freq2 = 0;
    let freq3 = 0;
    let freq4 = 0;
    let freq5 = 0;
    let freq6 = 0;
    
    //Question 2 here 
    
    let outPutArea = document.getElementById("outputArea2")
    for (let i=0; i<60000; i++)
        {
            var diceNum = Math.floor((Math.random() * 6) + 1)
            if (diceNum===1)
                {
                    freq1++;
                }
            else if (diceNum===2)
                {
                    freq2++;
                }
            else if (diceNum===3)
                {
                    freq3++
                }
            else if (diceNum===4)
                {
                    freq4++
                }
            else if (diceNum===5)
                {
                    freq5++
                }
            else if (diceNum===6)
                {
                    freq6++
                }
        }
        outPutArea.innerText = "Frequency of die rolls :"+ "\n" + "1: " + freq1 + "\n"+ "2: " + freq2 + "\n"+ "3: " + freq3 + "\n"+ "4: " + freq4 + "\n"+ "5: " + freq5 + "\n"+ "6: " + freq6 + "\n";
}

   
function question3(){
    let freq = [0,0,0,0,0,0,0];
    //Question 3 here 
    let outPutArea = document.getElementById("outputArea3")
    for (let i=0; i<60000; i++)
        {
            var diceNum = Math.floor((Math.random() * 6) + 1)
            freq[diceNum]++
        }
        outPutArea.innerText = "Frequency of die rolls :"+ "\n" + "1: " + freq[1] + "\n"+ "2: " + freq[2] + "\n"+ "3: " + freq[3] + "\n"+ "4: " + freq[4] + "\n"+ "5: " + freq[5] + "\n"+ "6: " + freq[6]+ "\n";
}

function question4(){
var dieRolls = {
    Frequencies: {
        1:0,
        2:0,
        3:0,
        4:0,
        5:0,
        6:0,
        },
    Total:60000,
    Exceptions: []
    }
let outPutArea = document.getElementById("outputArea4") 
for (let i=0; i<60000; i++)
    {
        var diceNum = Math.floor((Math.random() * 6) + 1)
        dieRolls.Frequencies[diceNum]++
    }
    
//for(let prop in dieRolls)
//{
//    if(dieRolls.Frequencies[prop] > 10100 || dieRolls.Frequencies[prop] < 9900)
//    {
//        dieRolls.Exceptions.push += prop
//    }
//}
   
for(let obj in dieRolls.Frequencies)
{
    let exc= 10000 - dieRolls.Frequencies[obj]

    if(Math.abs(exc) >= (dieRolls.Total/6*0.01))
    {
        dieRolls.Exceptions.push(obj)
    }
}

    //Question 4 here 
    
    
    outPutArea.innerText = "Frequency of die rolls :"+ "\n" + "Total rolls: " + dieRolls['Total'] + "\n" + "Frequencies:" + "\n" + "1: " + dieRolls.Frequencies[0] + "\n"+ "2: " + dieRolls.Frequencies[1] + "\n"+ "3: " + dieRolls.Frequencies[2] + "\n"+ "4: " + dieRolls.Frequencies[3] + "\n"+ "5: " + dieRolls.Frequencies[4] + "\n"+ "6: " + dieRolls.Frequencies[5] + "\n" + "Exceptions: " + dieRolls.Exceptions;

}

function question5(){
    let person = {
        name: "Jane",
        income: 127050
    }
    //Question 5 here 
    
    let outPutArea = document.getElementById("outputArea5")
    
    let personIncome = person['income']
    if (person['income']<=18200)
        {
            tax = 0
        }
    else if (person['income']>18200 && person['income']<37000)
        {
            tax = (personIncome-18200)*0.19
        }
    else if (person['income']>37001 && person['income']<90000)
        {
            tax = 3572+(personIncome-37000)*0.325
        }
    else if (person['income']>90001 && person['income']<180000)
        {
            tax = 20797+(personIncome-90000)*0.37
        }
    else if (person['income']>180001)
        {
            tax = 54097+(personIncome-180000)*0.45
        }
    else
        {
            
        }
        
    
    outPutArea.innerText = "Jane’s income is: $" + person['income'] + ", and her tax owed is: $" + tax
}