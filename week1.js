//task 1
let radius=4
let c=2*Math.PI*radius
let v= c.toFixed(2)
console.log(v)


//task 2
let animalString = "cameldogcatlizard";
let andString = " and ";
//to take the cat words
let catString=animalString.substring(8,11) 
//to take the dog words
let dogString=animalString.substr(5,3) 
console.log(catString + andString + dogString)


//task 3
let bio = 
{
  firstName: "Kanye",
  lastName: "West",
  birthDate: "8 June 1977",
  annualIncome: 150000000.00
};
console.log(bio["firstName"]+ " "+ bio["lastName"] + " was born on" + " and has an annual income of" + " $" + bio["annualIncome"])
  
  
//task 4
var number1, number2;
//RHS generates a random number between 1 and 10 inclusive
number1 = Math.floor((Math.random() * 10) + 1);
//RHS generates a random number between 1 and 10 inclusive
number2 = Math.floor((Math.random() * 10) + 1);
console.log("number1 = " + number1 + " number2 = " + number2);

//then we swap the number1 to number2, and vice versa
numTemp = number1
number1 = number2
number2 = numTemp
//HERE your code to swap the values in number1 and number2
console.log("number1 = " + number1 +" number2 = " + number2 );


//task 5
let year;
let yearNot2015Or2016;
year = 2015;
yearNot2015Or2016 = year !== 2015 && year !== 2016;
console.log(yearNot2015Or2016);

//extra question
let favFood = ["Fried noodle","Fried rice","Roast pork","Satay","Beef rendang"]
let priceFood = ["$10","$11","$15","$14","$12"]
console.log(favFood[0]+" = "+priceFood[0])
console.log(favFood[1]+" = "+priceFood[1])
console.log(favFood[2]+" = "+priceFood[2])
console.log(favFood[3]+" = "+priceFood[3])
console.log(favFood[4]+" = "+priceFood[4])